package sistema_bancario_basico;

import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class Main {

	// Vetores
	public static String vetNome[] = new String[10];
	public static long vetCPF[] = new long[10];
	public static String vetSenha[] = new String[10];
	public static String vetConta[] = new String[10];
	public static double vetSaldo[] = new double[10];
	public static double vetDeposito[] = new double[100];
	public static double vetTransferencia[] = new double[100];
	public static double vetSaque[] = new double[100];
	public static String vetExtrato[] = new String[100];
	
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		
		// Iniciando o vetor com valores negativos.
		for (int i = 0; i < vetExtrato.length; i++) {
			vetExtrato[i] = "";
		}
		
		telaPrincipal();
		System.out.println("Programa Encerrado!!!");
		sc.close();
	}

	public static void telaPrincipal() {

		boolean flag = true;
		
		String opcao = "";

		do {
			System.out.println("_________________");
			System.out.println("Sistema Bancário - DIGDOG");
			System.out.println();
			System.out.println("Criar Conta [ 1 ]");
			System.out.println("Fazer Login [ 2 ]");
			System.out.println("Sair        [ 9 ]");
			System.out.println("_________________");

			System.out.print("Digite uma opção: ");
			opcao = sc.next();

			if (!(opcao.equals("1") || opcao.equals("2") || opcao.equals("9"))) {
				System.out.println("Opção inválida. Tente novamente...");
			}

			switch (opcao) {
			case "1":
				criarConta();
				break;

			case "2":
				flag = fazerLogin();
				break;

			case "9":
				flag = false;
				break;
			default:
				break;
			}
		} while (flag);
	}

	public static boolean fazerLogin() {
		
		boolean processo = true;

		for (int i = 0; i < 3; i++) {
			System.out.println();

			System.out.print("CONTA: ");
			String lConta = sc.next();

			System.out.print("SENHA: ");
			String lSenha = sc.next();
			
			vetSaldo[i] = (double) 0;

			for (int j = 0; j < vetConta.length; j++) { 
				if (lConta.equals(vetConta[j])) {
					for (int j2 = 0; j2 < vetSenha.length; j2++) {
						if (!(lSenha.equals(vetSenha[j2]))) {
							System.out.println("Senha inválida... Tente novamente.");
							j = vetConta.length;
							i = 3;
						} else {
							System.out.println("Login efetuado com sucesso!!!");
							processo = subMenu(j);
							i = 3;
						}
					}
				} else {
					System.out.println("Conta inexistente ou inválida!!!");
					j = vetConta.length;
					i = 3;
				}
			}
		}
		System.out.println("Conta/Senha inválidas ou inexistentes.");
		return processo;
	}

	public static boolean subMenu(int index) {

		String lOpcao = "";
		boolean flag = true;

		do {
			System.out.println();
			System.out.printf("Seja Bem-vindo Sr(a) %s, Conta: %s, Saldo: R$%.2f %n", vetNome[index], vetConta[index], vetSaldo[index]);
			System.out.println("--------------------------");
			System.out.println("SACAR                 - [1]");
			System.out.println("DEPOSITAR             - [2]");
			System.out.println("TRANSFERIR            - [3]");
			System.out.println("EXTRATO               - [4]");
			System.out.println("CONSULTAR DADOS CONTA - [5]");
			System.out.println("SAIR                  - [9]");
			System.out.println();
			
			System.out.print("Informe uma opção: ");
			lOpcao = sc.next();

			if (!(lOpcao.equals("1") || (lOpcao.equals("2") || lOpcao.equals("5") ||
					(lOpcao.equals("3") || (lOpcao.equals("4") || (lOpcao.equals("9"))))))) {
				System.out.println("Opção inválida!!!");
			}

			switch (lOpcao) {
			case "1":
				sacar(index);
				break;
			case "2":
				depositar(index);
				break;
			case "3":
				transferir(index);
				break;
			case "4":
				extrato(index);
				break;
			case "5":
				consultaDados(index);
				break;
			case "9":
				flag = false;
				break;
			default:
				System.out.println();
				System.out.println("Opção inválida...");
				break;
			}
		} while (flag);
		return false;
	}

	public static boolean criarConta() {

		boolean flag = true;
		int lCont = 0;
		int lErro = 3;
		boolean processo = false;

		for (int i = 0; i < vetNome.length; i++) {
			if (vetNome[i] != null) {
				lCont += i;
				break;
			}
		}

		while (flag) {
			System.out.println();
			System.out.print("Informe o seu nome completo: ");
			vetNome[lCont] = sc.next();
			
			System.out.print("Informe o seu CPF (apenas números): ");
			vetCPF[lCont] = sc.nextLong();

			vetConta[lCont] = contaRandom();

			for (int i = 0; i <= 2; i++) {

				System.out.print("Informe sua senha: ");
				vetSenha[lCont] = sc.next();
				System.out.print("Repita sua senha: ");
				String lSenhaAux = sc.next();

				if (!(vetSenha[lCont].equals(lSenhaAux))) {
					if (i == 2) {
						flag = false;
						processo = flag;
						System.out.println();
						System.out.print("Tentativas expiradas...");
						System.out.println(" Você será redirecionado para a tela principal");
					}
					System.out.println();
					System.out.println("Senha digitada é diferente!!!");
					System.out.printf("Você possui mais %d tentativas...", --lErro);
				} else {
					System.out.println();
					System.out.println("Conta criada com sucesso!!!");
					exibirConta(vetNome[i], vetCPF[i], vetSenha[i], vetConta[i]);
					System.out.println("Você será redirecionado para a tela principal...");
					flag = false;
					processo = true;
					i = 2;
				}
			}
			System.out.println();
			lCont += 1;
		}
		return processo;
	}

	public static void sacar(int index) {
		
		double lSaque = 0.00;
		int lCont = 0;
		
		for (int i = 0; i < vetExtrato.length; i++) {
			if (vetExtrato[i].equals("")) {
				lCont = i;
				i = vetExtrato.length; // Para o for na posição atual
			}
		}
		
		System.out.print("Informe o valor para saque: ");
		lSaque = sc.nextDouble();
		
		if (vetSaldo[index] == 0.00) {
			System.out.println("Saldo igual ou inferior a zero.");
			System.out.println("Não será possível sacar o dinheiro ou transferir");
		} else if ((vetSaldo[index] - lSaque) < 0.00 || lSaque <= 0.00) {
			System.out.println("Valor do saque maior que o saldo da conta.");
			System.out.println("Tente um valor menor ou igual ao saldo da conta.");
		} else {
			vetExtrato[lCont] = "Saque R$ " + lSaque;
			vetSaldo[index] -= lSaque;
			System.out.println("Saque efetuado com sucesso.");
			System.out.println("Novo saldo: R$ " + vetSaldo[index]);
		}
	}

	public static void depositar(int index) {
		
		double lSaldo = 0.00;
		int lCont = 0;
		
		for (int i = 0; i < vetExtrato.length; i++) {
			if (vetExtrato[i].equals("")) {
				lCont = i;
				i = vetExtrato.length; // Para o for na posição atual
			}
		}
		
		System.out.println();
		System.out.print("Digite o valor a depositar em sua conta: ");
		lSaldo = sc.nextDouble();
		
		if (lSaldo <= 0.00) {
			System.out.println("Valor a depositar inválido.");
		} else {
			vetSaldo[index] += lSaldo;
			vetExtrato[lCont] = "Deposito R$ " + lSaldo;
			System.out.println("Deposito realizado com sucesso!");
			System.out.println("Novo Saldo: R$ " + vetSaldo[index]);
		}
	}

	public static void extrato(int index) {
		System.out.println(" ------------------");
		System.out.println("| EXTRATO BANCÁRIO |");
		System.out.println(" ------------------");
		
		for (int i = 0; i < vetExtrato.length; i++) {
			if (!(vetExtrato[i].equals(""))) {
				System.out.println(" " + vetExtrato[i]);
			}
		}
	}

	public static void transferir(int index) {
		
		double lTransf = 0.00;
		int lCont = 0;
	
		for (int i = 0; i < vetExtrato.length; i++) {
			if (vetExtrato[i].equals("")) {
				lCont = i;
				i = vetExtrato.length; // Para o for na posição atual
			}
		}
		
		System.out.print("Digite o valor a transferir: ");
		lTransf = sc.nextDouble();
		
		if (vetSaldo[index] == 0.00) {
			System.out.println("Você não possui saldo!");
		} else if (lTransf <= 0.00) {
			System.out.println("Valor para transferência igual ou menor do que zero");
		} else if ((vetSaldo[index] - lTransf) < 0) {
			System.out.println("Você não possui saldo suficiente!");
		} else {
			vetSaldo[index] -= lTransf;
			vetExtrato[lCont] = "Transferência R$ " + lTransf;
			System.out.println("Transferência realizada com sucesso!");
			System.out.println("Saldo em conta atual: R$ " + vetSaldo[index]);
		}
	}

	public static void exibirConta(String nome, long cpf, String senha, String conta) {
		System.out.println();
		System.out.printf("NOME : %s%n", nome);
		System.out.printf("CONTA: %s%n", conta);
		System.out.printf("CPF  : %d%n", cpf);
		System.out.printf("Senha: %s%n", senha);
		System.out.println();
	}

	public static String contaRandom() {
		Random r = new Random();

		int conta = r.nextInt(1000) * 10;
		int digito = r.nextInt(10);

		return String.valueOf(conta) + "-" + String.valueOf(digito);
	}

	public static void consultaDados( int i ) {
		exibirConta(vetNome[i], vetCPF[i], vetSenha[i], vetConta[i]);

		System.out.println("Você será redirecionado para a tela principal...");
		subMenu(i);
	}
}